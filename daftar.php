<?php 
	session_start();
	require 'koneksi.php';

	if(isset($_SESSION["login"])) {
		header("Location:admin.php");
		exit;
	}

	if(isset($_POST['daftar'])) {
		$username = strtolower(stripslashes($_POST["username"]));
		$password = mysqli_real_escape_string($conn, $_POST["password"]);
		$hashed_password = password_hash($password, PASSWORD_DEFAULT);

		$qdaftar = "INSERT INTO user VALUES('', '$username', '" .$_POST['nama']. "','" .$_POST['nohp']. "','$hashed_password')";

		if($conn->query($qdaftar)===TRUE) {
			$conn->close();
			header("location:login.php");
			exit();
		}
		else {
			echo"Error:".$sql."<br>".$conn->error;
			$conn->close();
			exit();
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Daftar - Lokerku</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<body class="background">
	<div class="container">
		<div class="row vh-100 align-items-center justify-content-center">
			<div class="col-sm-8 col-md-6 col-lg-4 bg-white rounded p-4 shadow">
				<div class="row justify-content-center mb-4">
					<img src="images/logo_biru.png" class="w-25">
				</div>
				<form action="" method="POST">
					<div class="mb-4">
						<label for="nama" class="form-label">Nama</label>
						<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama Kamu" aria-describedby="emailHelp" required>
					</div>
					<div class="mb-4">
						<label for="username" class="form-label">Username</label>
						<input type="text" class="form-control" name="username" id="username" placeholder="Masukan Username Kamu" aria-describedby="emailHelp" required>
					</div>
					<div class="mb-4">
						<label for="nohp" class="form-label">Nomer Telephone</label>
						<input type="text" class="form-control" name="nohp" id="nohp" placeholder="Masukan No HP Kamu" aria-describedby="emailHelp" required>
					</div>
					<div class="mb-4">
						<label for="password" class="form-label">Password</label>
						<input type="password" class="form-control" name="password" id="pass" placeholder="Masukan Password Kamu" aria-describedby="passwordHelp" required>
					</div>
					<button type="submit" class="btn btn-success w-100 mb-3" name="daftar">Daftar</a>
				</form>
				<a href="login.php" role="button"><button class="btn btn-light w-100">Login</button></a>
			</div>
		</div>
	</div>

</body>
</html>