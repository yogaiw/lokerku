<?php
	include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lokerku</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-sm justify-content-start">
		<div class="container">
			<a class="navbar-brand" href="index.php"><img src="images/logo.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
				<ul class="navbar-nav text-right">
					<li class="nav-item active">
						<a class="nav-link" href="masuk.php">Masuk</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="daftar.php">Daftar</a>
					</li>
					<button class="btn btn-light ml-auto"><a class="perusahaan">Perusahaan</a></button>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Header -->
	<header class="py-5">
		<div class="container">
			<h1 class="display-4 text-white"><center>Cari Pekerjaan Lebih Mudah dengan LOKERKU</center></h1>
		</div>
	</header>

	<!-- Lowongan Kerja Populer -->
	<div class="lowongan">
		<div class="container">
			<div class="col-sm-12">
				<div class="today-special">
					<h4>Lowongan Kerja Terpopuler</h4>
					<div class="row">
						<div class="col-sm-3">
							<div class="card card-one" style="width: 14rem;">
								<img class="card-img-top" src="images/lowongan2.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Indosat Oredoo</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan1.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Evos E-Sport</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan3.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Req Regum Qeon</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan4.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Tokopedia</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row kedua">
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan2.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Indosat Oredoo</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan1.jpg" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">Evos E-Sport</h5>
									<p class="card-text">Web Developer</p>
									<div class="button-group" role="group">
										<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
										<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<a href="index.html" class="link">
								<div class="card card-one">
									<img class="card-img-top" src="images/lowongan3.jpg" alt="Card image cap">
									<div class="card-body">
										<h5 class="card-title">Req Regum Qeon</h5>
										<p class="card-text">Web Developer</p>
										<div class="button-group" role="group">
											<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
											<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-sm-3">
							<a href="index.html" class="link">
								<div class="card card-one">
									<img class="card-img-top" src="images/lowongan4.jpg" alt="Card image cap">
									<div class="card-body">
										<h5 class="card-title">Tokopedia</h5>
										<p class="card-text">Web Developer</p>
										<div class="button-group" role="group">
											<button type="button" class="btn btn-sm btn-outline-primary">Detail</button>
											<button type="button" class="btn btn-sm btn-primary">Apply Now</button>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="text-center">
						<a href="list_loker.php">
						<button type='button' class='btn'> Lihat Semua Lowongan</button>
						</a>
					</div>
				</div>
			</div>   
		</div>
	</div>

	<!-- Perusahaan -->
	<div class="lowongan">
		<div class="container">
			<div class="col-sm-12">
				<div class="today-special">
					<h4><center>Perusahaan Rekomnedasi Kami</center></h4>
					<p><center>Perusahaan-perusahaan terbaik ini mencari talenta seperti Anda</center></p>
					<div class="row">
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan2.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-two">
								<img class="card-img-top" src="images/lowongan1.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan3.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-two">
								<img class="card-img-top" src="images/lowongan4.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
					</div>
					<div class="row kedua">
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan2.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-two">
								<img class="card-img-top" src="images/lowongan1.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-one">
								<img class="card-img-top" src="images/lowongan3.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card card-two">
								<img class="card-img-top" src="images/lowongan4.jpg" alt="Card image cap">
								<div class="card-body">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>   
		</div>
	</div>

	<!-- Footer -->
	<footer class="footer">
		<div class="copyright">
			© 2021 Lokerku
		</div>
	</footer>

	<script src="js/bootstrap.min.js"></script>
</body>
</html>