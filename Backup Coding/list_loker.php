<?php
	include "koneksi.php";

	$lokerlist = $conn->query(
        "SELECT * FROM loker
        LEFT JOIN perusahaan ON loker.perusahaan_id = perusahaan.perusahaan_id
        LEFT JOIN user ON loker.user_id = user.user_id"
    );
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lokerku</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-sm justify-content-start">
		<div class="container">
			<a class="navbar-brand" href="index.php"><img src="images/logo.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
				<ul class="navbar-nav text-right">
					<li class="nav-item active">
						<a class="nav-link" href="masuk.php">Masuk</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="daftar.php">Daftar</a>
					</li>
					<button class="btn btn-light ml-auto"><a class="perusahaan">Perusahaan</a></button>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Header -->
	<header class="py-5">
		<div class="container">
			<h1 class="display-4 text-white"><center>Cari Pekerjaan Lebih Mudah dengan LOKERKU</center></h1>
		</div>
	</header>

	<!-- Lowongan Kerja Populer -->
	<div class="list">
		<div class="col-sm-12">
			<div class="today-special">
				<h4>List Lowongan Pekerjaan</h4>
			</div>
		</div>
	</div>
	
	<?php foreach($lokerlist as $index=>$value): ?>
	<div class="card">
		<div class="content">
			<img src="images/lowongan1.jpg">
			<div class="text">
				<h3><strong><?= $value['nama_perusahaan'] ?></strong></h3>
				<p><?= $value['posisi_loker'] ?></p>
			</div>
			<div class="flex-end">
				<a href="#" class="btn btn-success">Lamar</a>
			</div>
		</div>
	</div>
	<?php endforeach; ?>

	
	<!-- Footer -->
	<footer class="footer">
		<div class="copyright">
			© 2021 Lokerku
		</div>
	</footer>

	<script src="js/bootstrap.min.js"></script>
</body>
</html>