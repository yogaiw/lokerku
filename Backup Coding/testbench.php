<?php
    include "koneksi.php";

    $lokerlist = $conn->query(
        "SELECT * FROM loker
        LEFT JOIN perusahaan ON loker.perusahaan_id = perusahaan.perusahaan_id
        LEFT JOIN user ON loker.user_id = user.user_id"
    );
?>
<!DOCTYPE html>
<html>
<head>
    <title>TESTING DATABASE</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No. </th>
                <th scope="col">Judul Lowongan</th>
                <th scope="col">Posisi</th>
                <th scope="col">Perusahaan</th>
                <th scope="col">Created by</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($lokerlist as $index=>$value): ?>
            <tr>
                <th scope="row"></th>
                <td><?= $value['judul_loker'] ?></td>
                <td><?= $value['posisi_loker'] ?></td>
                <td><?= $value['nama_perusahaan'] ?></td>
                <td><?= $value['username'] ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>