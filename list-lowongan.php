<?php 
	require 'koneksi.php';

	$getLokerList =  $conn->query("SELECT * FROM loker LEFT JOIN user ON loker.created_by = user.user_id");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lokerku - Pencari Kerja?</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<body>
	
	<section id="header">
		<div class="menu-bar">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
					<a class="navbar-brand" href="index.php"><img src="images/logo.png"></a>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="login.php">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="daftar.php">Daftar</a>
							</li>
							<li class="nav-item">
								<a href="admin.php" type="button" class="btn btn-light button-user">User Profil</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="banner text-center">
			<h1>Cari Pekerjaan Lebih Mudah dengan LOKERKU!</h1>
		</div>
	</section>

	<div class="search-job text-center">
		<input type="text" class="form-control" placeholder="Pencarian">
		<input type="text" class="form-control" placeholder="Lokasi">
		<input type="button" class="btn btn-primary" value="Cari Pekerjaan">
	</div>

	<section id="jobs_lowongan">
		<div class="container">
			<h5>List Lowongan Pekerjaan</h5>
			<?php foreach($getLokerList as $index=>$value): ?>
			<div class="company-details-lowongan mb-5">
				<div class="job-update-lowongan">
					<h4><b><?= $value["posisi"] ?></b></h4>
					<p><?= $value["nama_perusahaan"] ?></p>
					<i class="fa fa-briefcase"></i><span><?= $value["gaji"] ?></span><br>
					<i class="fa fa-map"></i><span><?= $value["alamat"] ?></span><br>
					<i class="fa fa-whatsapp"></i><span><?= $value["wa"] ?></span><br>
					<i class="fa fa-user"></i><span>created by : <?= $value["nama"] ?></span><br>
				</div>
				<div class="apply-btn">
					<button type="button" class="btn btn-primary">Apply</button>
				</div>
				<?php endforeach; ?>				
			</div>
		</div>
	</section>

	<section id="footer" class="text-center">
		<img src="images/logo.png" class="footer-image">
		<p>Lokerku adalah website pencari pekerjaan terbaik yang pernah ada didunia ini wow amazing aku cinta indomaret</p>
		<hr>
		<p>Made With <i class="fa fa-heart"></i> © Lokerku - 2021</p>
	</section>

	<script src="js/bootstrap.min.js"></script>
</body>
</html>