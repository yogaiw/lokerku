<?php 
	session_start();
	require 'koneksi.php';

	if(isset($_SESSION["login"])) {
		header("Location:admin.php");
		exit;
	}

	if( isset($_POST['login']) ) {
		$username = strtolower(stripslashes($_POST["username"]));
		$password = mysqli_real_escape_string($conn, $_POST['password']);

		$cek_login = $conn->query("SELECT * FROM user WHERE username = '$username'");
		$ktm_login = $cek_login->num_rows;
		$data_login = $cek_login->fetch_assoc();

		if($ktm_login === 1) {
			if(password_verify($password,$data_login['password'])){
				$_SESSION["login"] = true;
				$_SESSION['current_user'] = $data_login['user_id'];
				header("Location:admin.php");
				exit;
			}
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login - Lokerku</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<body class="background">
	<div class="container">
		<div class="row vh-100 align-items-center justify-content-center">
			<div class="col-sm-8 col-md-6 col-lg-4 bg-white rounded p-4 shadow">
				<div class="row justify-content-center mb-4">
					<img src="images/logo_biru.png" class="w-25">
				</div>
				<form action="" method="POST">
					<div class="mb-4">
						<label for="username" class="form-label">Username</label>
						<input type="text" class="form-control" name="username" id="username" placeholder="Masukan Username Kamu" aria-describedby="emailHelp">
					</div>
					<div class="mb-4">
						<label for="password" class="form-label">Password</label>
						<input type="password" class="form-control" name="password" id="password" placeholder="Masukan Password Kamu" aria-describedby="passwordHelp">
					</div>
					<button type="submit" class="btn btn-success w-100 mb-3" name="login">Login</a> 
				</form>
				<a href="daftar.php" role="button"><button class="btn btn-light w-100"> Daftar</button></a>
			</div>
		</div>
	</div>
</body>
</html>