<?php 
	require 'koneksi.php';

	$getLokerList =  $conn->query("SELECT * FROM loker LEFT JOIN user ON loker.created_by = user.user_id");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lokerku - Pencari Kerja?</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<body>
	
	<section id="header">
		<div class="menu-bar">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
					<a class="navbar-brand" href="index.php"><img src="images/logo.png"></a>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="login.php">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="daftar.php">Daftar</a>
							</li>
							<li class="nav-item">
								<a href="admin.php" type="button" class="btn btn-light button-user">User Profil</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="banner text-center">
			<h1>Cari Pekerjaan Lebih Mudah dengan LOKERKU!</h1>
		</div>
	</section>

	<div class="search-job text-center">
		<input type="text" class="form-control" placeholder="Pencarian">
		<input type="text" class="form-control" placeholder="Lokasi">
		<input type="button" class="btn btn-primary" value="Cari Pekerjaan">
	</div>

	<section id="recruiters">
		<div class="container text-center">
			<h3>Perusahaan Rekomendasi Kami</h3>
			<div>
				<img src="images/ps1.png">
				<img src="images/ps2.png">
				<img src="images/ps3.png">
				<img src="images/ps4.png">
				<img src="images/ps5.png">
				<img src="images/ps6.png">
			</div>
			<div>
				<img src="images/ps13.png">
				<img src="images/ps14.png">
				<img src="images/ps15.png">
				<img src="images/ps16.png">
				<img src="images/ps17.png">
				<img src="images/ps18.png">
			</div>
			<div>
				<img src="images/ps7.png">
				<img src="images/ps8.png">
				<img src="images/ps9.png">
				<img src="images/ps10.png">
				<img src="images/ps11.png">
				<img src="images/ps12.png">
			</div>
		</div>
	</section>

	<section id="jobs">
		<div class="container">
			<h5>List Lowongan Pekerjaan</h5>
			<?php foreach($getLokerList as $index=>$value): ?>
			<div class="company-details">
				<div class="job-update">
					<h4><b><?= $value["posisi"] ?></b></h4>
					<p><?= $value["nama_perusahaan"] ?></p>
					<i class="fa fa-briefcase"></i><span><?= $value["gaji"] ?></span><br>
					<i class="fa fa-map"></i><span><?= $value["alamat"] ?></span><br>
					<i class="fa fa-whatsapp"></i><span><?= $value["wa"] ?></span><br>
					<i class="fa fa-user"></i><span>created by : <?= $value["nama"] ?></span><br>
				</div>
				<div class="apply-btn">
					<button type="button" class="btn btn-primary">Apply</button>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
			<div class="d-grid gap-2 col-6 mx-auto text-center">
				<a href="list-lowongan.php" type="button" class="btn btn-success">Lihat Semua Lowongan</a>
			</div>
		</div>
	</section>

	<section id="site-stats">
		<div class="container text-center">
			<h3>Kemudahan Menggunakan Lokerku</h3>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-6">
							<div class="stats-box">
								<i class="fa fa-user"></i><span><small>720 ++</small></span>
								<p>Pengguna</p>
							</div>
						</div>
						<div class="col-6">
							<div class="stats-box">
								<i class="fa fa-slideshare"></i><span><small>320 ++</small></span>
								<p>Koneksi</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-6">
							<div class="stats-box">
								<i class="fa fa-hand-peace-o"></i><span><small>540 ++</small></span>
								<p>Lowongan</p>
							</div>
						</div>
						<div class="col-6">
							<div class="stats-box">
								<i class="fa fa-building-o"></i><span><small>500 ++</small></span>
								<p>Perusahaan</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="footer" class="text-center">
		<img src="images/logo.png" class="footer-image">
		<p>Lokerku adalah website pencari pekerjaan terbaik yang pernah ada didunia ini wow amazing aku cinta indomaret</p>
		<hr>
		<p>Made With <i class="fa fa-heart"></i> © Lokerku - 2021</p>
	</section>

	<script src="js/bootstrap.min.js"></script>
</body>
</html>